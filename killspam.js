/* eslint-disable no-console */
const Matrix = require('matrix-js-sdk');
const Config = require('./config.js');

function getConfig(x) {
  // src https://stackoverflow.com/questions/37511572/how-to-convert-a-string-to-a-symbol-in-javascript
  const config = Config[x];
  if(config == undefined) { return []; }
  else { return config; }
}

const main = async () => {

  const args = process.argv.slice(1);
  const arg = args[1];
  let removeContent = false;
  let intention = 'I would';
  if(arg === '-d') {
    removeContent = true;
    intention = 'I\'m going to';
  }

  const allAuthors = getConfig('allAuthors');
  const allRegex = getConfig('allRegex');
  const allRooms = getConfig('allRooms');
  const histLimit = getConfig('historyLimit');

  console.log(intention + ' remove last ' + histLimit + ' messages in:' +
      '\n  rooms: [' + allRooms + ']\n  with:' +
      '\n    authors: [' + allAuthors + ']' +
      '\n    regex: [' + allRegex + ']');

  const client = Matrix.createClient({
    baseUrl: 'https://' + Config.userName.split(':')[1],
    accessToken: Config.accessToken,
    userId: Config.userName
  });

  await new Promise((resolve, reject) => {
    client.on('sync', (state, prevState, data) => {
      if (state === 'ERROR') {
        console.log(data);
        return reject('Error syncing');
      }
      if (state === 'PREPARED') {
        return resolve();
      }
    });
    client.startClient();
  });

  await Promise.all(allRooms.map(async (roomName) => {
    let room;
    if (roomName[0] == '!') {
      room = client.getRoom(roomName);
    } else { // already internal room (starts with !)
      const id = await client.getRoomIdForAlias(roomName);
      console.log('Internal ID of',roomName,'is', id.room_id);
      room = client.getRoom(id.room_id);
    }

    console.log('Getting history from', roomName);
    let events;
    await client.scrollback(room, Config.historyLimit, (err, evts) => {
      if (err) { throw err; }
      if (evts.chunk) {
        if (events) {
          throw new Error('Events is already defined');
        }
        events = evts;
      }
    });

    console.log('Got history from', roomName);
    // I have no clue why we need both the events query and the LiveTimeline but without
    // them both, the list is incomplete.
    const allEvents = [
      ...(events.chunk || []),
      ...room.getLiveTimeline().getEvents().map(e => (e.event))
    ];

    await Promise.all(allEvents.map(async (event) => {
      let content = JSON.stringify(event.content);

      if (event.type !== 'm.room.message' &&
            // in this last attack, we don't see messages (filtered?) but a lot of strange logins from IRC => membership is relevant
            event.type !== 'm.room.member') {
        return;
      }

      // src https://www.w3schools.com/jsref/jsref_includes_array.asp
      if (!allAuthors.includes(event.sender) &&
            // src https://stackoverflow.com/a/38075457
            !allRegex.some((rx) => rx.test(content))) {
        return;
      }

      if (content === '{}') { return; }

      const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

      const doit = async () => {
        if (removeContent) {
          try {
            await client.redactEvent(room.roomId, event.event_id);
            console.log('Deleting from room', roomName, 'content:', content);
          } catch (err) {
            if (err.errcode === 'M_LIMIT_EXCEEDED') {
              console.log('M_LIMIT_EXCEEDED');
              await delay(err.data.retry_after_ms);
              return await doit();
            }
            console.error('Error redacting event', err);
          }
        } else {
          console.log('I would remove from room', roomName, 'the following content:', content);
        }
      };

      await doit();
    }));
  }));

  console.log('Done');
  client.stopClient();
};

(async () => {
  try {
    await main();
  } catch (e) {
    console.error(e);
  }
})();
