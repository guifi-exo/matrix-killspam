this project is a fork of https://github.com/xwiki-labs/matrix-stfu

# Matrix Killspam

Spam / Trolling Filtration Utility

![the title says it all](https://gitlab.com/guifi-exo/matrix-killspam/raw/master/killspam.jpg)

Mass remove last number of messages:

- some rooms (at least 1) - `allRooms`
- some authors (optional) - `allAuthors`
- some content (optional) - `allRegex`

## How to use

1. `cp ./config.example.js ./config.js`
2. Edit config.js to make it use your `userName`, `accessToken`, target rooms `allRooms`, target content `allRegex`, target authors `allAuthors`. You can also change how far into history of the progrma will search (number of events) in the variable `historyLimit`.
3. `node ./killspam.js` will simulate what would remove
4. `node ./killspam.js -d` is going to remove content (there is only availabe this option)

# Too much M_LIMIT_EXCEEDED

If you are reaching too much `M_LIMIT_EXCEEDED` you can disable it temporarily to delete spam fastly

edit file `/etc/matrix-synapse/homeserver.yaml` the line:

```
#rc_messages_per_second: 0.2
rc_messages_per_second: 200000
```

and restart server

    service matrix-synapse restart

I copied this value from here https://github.com/matrix-org/vagrant-synapse-test/blob/master/homeserver.yaml#L59
